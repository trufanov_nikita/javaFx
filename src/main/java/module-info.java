module com.example.taxi {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires org.controlsfx.controls;
    requires java.sql;

    opens com.example.taxi to javafx.fxml;
    exports com.example.taxi;
    exports com.example.taxi.controller;
    exports com.example.taxi.entity;
    opens com.example.taxi.controller to javafx.fxml;
}