/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.taxi.controller;

import com.example.taxi.alert.AlertHelper;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.example.taxi.db.DBManager;
import com.example.taxi.entity.Role;
import com.example.taxi.exception.ApplicationException;
import com.example.taxi.session.UserSession;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

public class RegisterController implements Initializable {
    @FXML
    private TextField name;

    @FXML
    private TextField email;

    @FXML
    private TextField password;

    @FXML
    private Button registerButton;

    Window window;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }


    @FXML
    private void register() throws ApplicationException, SQLException, ClassNotFoundException, IOException {
        if(isValidated()) {
            DBManager dbManager = DBManager.getInstance();
            if(dbManager.findUser(email.getText()) == null) {
                Stage stage = (Stage) registerButton.getScene().getWindow();
                stage.close();
                dbManager.addUser(name.getText(), email.getText(), Role.CLIENT.toString(), password.getText());
                UserSession.getInstance(email.getText(), Role.CLIENT);
                Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("UserView.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.setTitle("User Interface");
                stage.show();
            }
            else{
                AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                        "Email is busy.");
                email.requestFocus();
            }

        }

    }


    private boolean isValidated() {
        window = registerButton.getScene().getWindow();
        if (name.getText().equals("")) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "First name text field cannot be blank.");
            name.requestFocus();
        } else if (name.getText().length() < 2 || name.getText().length() > 25) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "First name text field cannot be less than 2 and greater than 25 characters.");
            name.requestFocus();
        }  else if (email.getText().equals("")) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Email text field cannot be blank.");
            email.requestFocus();
        } else if (email.getText().length() < 5 || email.getText().length() > 45) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Email text field cannot be less than 5 and greater than 45 characters.");
            email.requestFocus();
        } else if (password.getText().equals("")) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Password text field cannot be blank.");
            password.requestFocus();
        } else if (password.getText().length() < 3 || password.getText().length() > 25) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Password text field cannot be less than 3 and greater than 25 characters.");
            password.requestFocus();
        } else {
            return true;
        }
        return false;
    }

    private void clearForm() {
        name.clear();
        email.clear();
        password.clear();
    }

    @FXML
    private void showLoginStage() throws IOException {
        Stage stage = (Stage) registerButton.getScene().getWindow();
        stage.close();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("LoginView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("User Login");
        stage.show();
    }


}
