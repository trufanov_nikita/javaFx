package com.example.taxi.controller;

import com.example.taxi.alert.AlertHelper;
import com.example.taxi.db.DBManager;
import com.example.taxi.entity.Role;
import com.example.taxi.exception.ApplicationException;
import com.example.taxi.session.UserSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.sql.SQLException;

public class AdminController {
    @FXML
    TextField carNumber;

    @FXML
    TextField email;

    @FXML
    TextField password;

    @FXML
    TextField name;

    Window window;

    public void addDriver(ActionEvent actionEvent) throws SQLException, ClassNotFoundException, ApplicationException {
        DBManager db = DBManager.getInstance();
        db.addCar(carNumber.getText());
        db.addUser(name.getText(), email.getText(), String.valueOf(Role.DRIVER), password.getText());
        db.addDriver(db.findCar(carNumber.getText()).getId(),db.findUser(email.getText()).getId(), 50);
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, window, "Success",
                "New driver has been added");
    }

    @FXML
    public void logout(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) email.getScene().getWindow();
        stage.close();
        UserSession.clear();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("LoginView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("User Login");
        stage.show();
    }
}
