package com.example.taxi.controller;

import com.example.taxi.alert.AlertHelper;
import com.example.taxi.db.DBManager;
import com.example.taxi.exception.ApplicationException;
import com.example.taxi.session.UserSession;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.sql.SQLException;

public class UserController {
    @FXML
    public TextField addressFrom;

    @FXML
    public TextField addressTo;

    @FXML
    public Button submitButton;

    Window window;

    @FXML
    public void submit(ActionEvent actionEvent) throws ApplicationException, SQLException, ClassNotFoundException {
        DBManager db = DBManager.getInstance();
        int userId = db.findUser(UserSession.getEmail()).getId();
        db.makeOrder(addressFrom.getText(), addressTo.getText(), userId);
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, window, "Success",
                "Your order has been added for drivers");
    }

    @FXML
    public void logout(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) submitButton.getScene().getWindow();
        stage.close();
        UserSession.clear();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("LoginView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("User Login");
        stage.show();
    }
}
