/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.taxi.controller;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.example.taxi.alert.AlertHelper;
import com.example.taxi.db.DBManager;
import com.example.taxi.entity.Role;
import com.example.taxi.entity.User;
import com.example.taxi.session.UserSession;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

public class LoginController implements Initializable {


    @FXML
    private TextField email;

    @FXML
    private TextField password;

    @FXML
    private Button loginButton;

    Window window;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public LoginController() {

    }

    @FXML
    private void login() throws Exception {
        if(!isValidated()){
            return;
        }
        DBManager db = DBManager.getInstance();
        User user = db.findUser(email.getText(), password.getText());

        if(user != null){
            UserSession.getInstance(user.getEmail(), user.getRole());
            Stage stage = (Stage) loginButton.getScene().getWindow();
            stage.close();
            Parent root = null;
            switch (user.getRole()){
                case DRIVER:
                    root = FXMLLoader.load(getClass().getClassLoader().getResource("DriverView.fxml"));
                    break;
                case ADMIN:
                    root = FXMLLoader.load(getClass().getClassLoader().getResource("AdminView.fxml"));
                    break;
                default:
                    root  = FXMLLoader.load(getClass().getClassLoader().getResource("UserView.fxml"));
                    break;
            }
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle(user.getRole().toString() + " Interface");
            stage.show();
        }
        else{
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Email or password are incorrect");
            email.requestFocus();
        }

    }

    private boolean isValidated() {

        window = loginButton.getScene().getWindow();
        if (email.getText().equals("")) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Email text field cannot be blank.");
            email.requestFocus();
        } else if (email.getText().length() < 5 || email.getText().length() > 25) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Email text field cannot be less than 5 and greater than 25 characters.");
            email.requestFocus();
        } else if (password.getText().equals("")) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Password text field cannot be blank.");
            password.requestFocus();
        } else if (password.getText().length() < 2 || password.getText().length() > 25) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, window, "Error",
                    "Password text field cannot be less than 5 and greater than 25 characters.");
            password.requestFocus();
        } else {
            return true;
        }
        return false;
    }

    @FXML
    private void showRegisterStage() throws IOException {
        Stage stage = (Stage) loginButton.getScene().getWindow();
        stage.close();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("RegisterView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("User Registration");
        stage.show();
    }
}
