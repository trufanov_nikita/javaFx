package com.example.taxi.controller;

import com.example.taxi.db.DBManager;
import com.example.taxi.entity.TicketModel;
import com.example.taxi.entity.User;
import com.example.taxi.exception.ApplicationException;
import com.example.taxi.session.UserSession;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class DriverController implements Initializable{
    boolean currentState = true; //if new orders -> true else false;
    @FXML
    public TableView<TicketModel> tableView;

    @FXML
    public TableColumn<TicketModel, SimpleIntegerProperty> id;

    @FXML
    public TableColumn<TicketModel, SimpleStringProperty> name;

    @FXML
    public TableColumn<TicketModel, SimpleStringProperty> email;

    @FXML
    public TableColumn<TicketModel, SimpleStringProperty> from;

    @FXML
    public TableColumn<TicketModel, SimpleStringProperty> to;

    @FXML
    public TableColumn<TicketModel, SimpleStringProperty> status;

    @FXML
    public TableColumn<TicketModel, Button> button;

    @FXML
    public Button changeTablesButton;

    @FXML
    public Label label;

    @FXML
    ObservableList<TicketModel> data;

    private Comparator<TicketModel> sortByStatus = (o1, o2) -> {
        if (o1.getStatus().equals("IN_PROGRESS") && o2.getStatus().equals("ENDED"))
            return -1;
        if (o1.getStatus().equals(o2.getStatus()))
            return 0;
        return 1;
    };


    @FXML
    public void refresh(ActionEvent actionEvent) {
        DBManager db = DBManager.getInstance();

        try {
            if(currentState){
                data = FXCollections.observableList(assignButtonActionForNewOrders(db, db.getTicketModelsByDriverId()));
            }
            else{
                data = FXCollections.observableList(assignButtonActionForCurrentOrders(db, db.getTicketModelsByDriverId(
                        db.findDriverId(
                                db.findUser(UserSession.getEmail()).getId()))));
                data.sort(sortByStatus);
            }

        } catch (SQLException | ClassNotFoundException | ApplicationException e) {
            throw new RuntimeException(e);
        }
        tableView.setItems(data);
        tableView.refresh();
    }



    @FXML
    public void changeTables(ActionEvent actionEvent) {
        DBManager db = DBManager.getInstance();
        currentState = !currentState;
        if(currentState){
            label.setText("New orders");
            changeTablesButton.setText("My orders");
        }
        else{
            label.setText("My orders");
            changeTablesButton.setText("New orders");
        }
        try {
            if(currentState){
                data = FXCollections.observableList(assignButtonActionForNewOrders(db, db.getTicketModelsByDriverId()));
            }
            else {
                data = FXCollections.observableList(assignButtonActionForCurrentOrders(db,
                        db.getTicketModelsByDriverId(
                                db.findDriverId(
                                        db.findUser(UserSession.getEmail()).getId()))));

                data.sort(sortByStatus);
            }
        } catch (SQLException | ClassNotFoundException | ApplicationException e) {
            throw new RuntimeException(e);
        }
        tableView.setItems(data);
        tableView.refresh();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        id.setCellValueFactory(new PropertyValueFactory<>("Id"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        email.setCellValueFactory(new PropertyValueFactory<>("Email"));
        from.setCellValueFactory(new PropertyValueFactory<>("From"));
        to.setCellValueFactory(new PropertyValueFactory<>("To"));
        status.setCellValueFactory(new PropertyValueFactory<>("Status"));
        button.setCellValueFactory(new PropertyValueFactory<>("Button"));
        label.setText("New orders");
        changeTablesButton.setText("My orders");
        DBManager db = DBManager.getInstance();
        try {
            data = FXCollections.observableList(assignButtonActionForNewOrders(db, db.getTicketModelsByDriverId()));
        } catch (SQLException | ClassNotFoundException | ApplicationException e) {
            throw new RuntimeException(e);
        }
        tableView.setItems(data);
        tableView.refresh();
    }
    private List<TicketModel> assignButtonActionForNewOrders(DBManager db, List<TicketModel> tickets) {
        tickets.forEach(x -> {
            Button assignAction = new Button();
            assignAction.setText("Accept");
            assignAction.setOnAction(actionEvent -> {
                try {
                    User user = db.findUser(UserSession.getEmail());
                    int driverId = db.findDriverId(user.getId());
                    db.acceptOrder(driverId, x.getId());
                    refresh(actionEvent);
                } catch (SQLException | ClassNotFoundException | ApplicationException e) {
                    throw new RuntimeException(e);
                }
            });
            x.setButton(assignAction);
        });
        return tickets;
    }

    private List<TicketModel> assignButtonActionForCurrentOrders(DBManager db, List<TicketModel> tickets) {
        tickets.forEach(x -> {
            Button assignAction = new Button();
            assignAction.setText("End");
            assignAction.setOnAction(actionEvent -> {
                        try {
                            db.endOrder(x.getId());
                            refresh(actionEvent);
                        } catch (SQLException | ClassNotFoundException | ApplicationException e) {
                            throw new RuntimeException(e);
                        }
                    }
            );
            x.setButton(assignAction);
        });
        return tickets;
    }

    @FXML
    public void logout(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) changeTablesButton.getScene().getWindow();
        stage.close();
        UserSession.clear();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("LoginView.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("User Login");
        stage.show();
    }
}

