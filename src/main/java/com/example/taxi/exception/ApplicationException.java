package com.example.taxi.exception;

public class ApplicationException extends Exception{
    public ApplicationException(String message, Exception exception){
        super(message, exception);
    }
}
