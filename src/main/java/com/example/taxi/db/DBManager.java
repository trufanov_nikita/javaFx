package com.example.taxi.db;

import com.example.taxi.entity.Car;
import com.example.taxi.entity.Role;
import com.example.taxi.entity.Ticket;
import com.example.taxi.entity.TicketModel;
import com.example.taxi.entity.User;
import com.example.taxi.exception.ApplicationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private static final String CONNECTION_URL = "jdbc:mysql://127.0.0.1:3306/taxi?user=root&password=baluha";
    private static DBManager instance;
    private Role[] roles = {Role.CLIENT, Role.DRIVER, Role.ADMIN};
    /**
     *
     * @return new DBManager if wasn't created or current DBManager
     */
    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }
    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Connection con = null;
        Class.forName("com.mysql.cj.jdbc.Driver");
        con = DriverManager.getConnection(CONNECTION_URL);
        con.setAutoCommit(false);
        return con;
    }

    public void addUser(String name, String email, String role, String password) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Insert INTO user (name, email, role, password)  Values (?,?,?,?)";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, name);
            stmt.setString(2, email);
            stmt.setString(3, role);
            stmt.setString(4, password);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }

    public User findUser(String email, String password, int role) throws SQLException, ClassNotFoundException, ApplicationException {
        String roleString = roles[role-1].toString();
        String query = "Select * from user where email = ? and password = ? and role = ?;";

        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, email);
            stmt.setString(2, password);
            stmt.setString(3, roleString);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), Role.valueOf(rs.getString(4)));
                break;
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return user;
    }
    public User findUser(String email, String password) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Select * from user where email = ? and password = ?;";

        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, email);
            stmt.setString(2, password);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), Role.valueOf(rs.getString(4)));
                break;
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return user;
    }
    public User findUser(String email) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Select * from user where email = ?;";
        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, email);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), Role.valueOf(rs.getString(4)));
                break;
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return user;
    }
    public Car findCar(String carNumber) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Select * from car where car_number = ?;";
        Car car = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, carNumber);
            rs = stmt.executeQuery();
            while (rs.next()) {
                car = new Car(rs.getInt(1), rs.getString(2), rs.getBoolean(3));
                break;
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return car;
    }
    public int findDriverId(int userId) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Select * from driver where user_id = ?;";
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, userId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return 0;
    }
    public User findUser(String email, String password, String role) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Select * from user where email = ? and password = ? and role = ?;";

        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(2, email);
            stmt.setString(3, password);
            stmt.setString(4, role);
            rs = stmt.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), Role.valueOf(rs.getString(4)));
                break;
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return user;
    }

    public void makeOrder(String from, String to, int userId) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "INSERT INTO ticket (user_id, `from`, `to`) VALUES (?, ?, ?);";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, userId);
            stmt.setString(2, from);
            stmt.setString(3, to);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    public void acceptOrder(int driverId, int orderId) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "UPDATE ticket SET driver_id = ?, status = 'IN_PROGRESS' WHERE (id = ?);";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, driverId);
            stmt.setInt(2, orderId);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    public void endOrder(int orderId) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "UPDATE ticket SET status = 'ENDED' WHERE (id = ?);";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, orderId);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    public void endOrder(int orderId, int feedback) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "UPDATE ticket SET status = 'ENDED', feedback = ? WHERE (id = ?);";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, feedback);
            stmt.setInt(2, orderId);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    public List<Ticket> getNewTickets() throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "SELECT * FROM taxi.ticket INNER JOIN user ON user_id=user.id where status = 'NEW';";
        List<Ticket> tickets = new ArrayList<>();
        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            rs = stmt.executeQuery();
            while (rs.next()) {
                tickets.add(new Ticket(rs.getInt(1), rs.getString(4), new User(rs.getInt(8), rs.getString(9), rs.getString(10), Role.valueOf(rs.getString(11))), rs.getString(6), rs.getString(7), rs.getInt(5)));
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return tickets;
    }
    public List<TicketModel> getTicketModelsByDriverId() throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "SELECT * FROM taxi.ticket INNER JOIN user ON user_id=user.id where status = 'NEW';";
        List<TicketModel> tickets = new ArrayList<>();
        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            rs = stmt.executeQuery();
            while (rs.next()) {
                tickets.add(new TicketModel
                        (new Ticket(rs.getInt(1), rs.getString(4),
                                new User(rs.getInt(8), rs.getString(9), rs.getString(10), Role.valueOf(rs.getString(11)))
                                , rs.getString(6), rs.getString(7), rs.getInt(5))));
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return tickets;
    }
    public List<TicketModel> getTicketModelsByDriverId(int driverId) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "SELECT * FROM taxi.ticket INNER JOIN user ON user_id=user.id where driver_id = ?;";
        List<TicketModel> tickets = new ArrayList<>();
        User user = null;
        Connection con = getConnection();
        ResultSet rs = null;
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, driverId);
            rs = stmt.executeQuery();
            while (rs.next()) {
                tickets.add(new TicketModel
                        (new Ticket(rs.getInt(1), rs.getString(4),
                                new User(rs.getInt(8), rs.getString(9), rs.getString(10), Role.valueOf(rs.getString(11)))
                                , rs.getString(6), rs.getString(7), rs.getInt(5))));
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
            close(rs);
        }
        return tickets;
    }
    public void addCar(String carNumber) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Insert INTO car (car_number)  Values (?)";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setString(1, carNumber);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    public void addDriver(int carId, int userId, int price) throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "Insert INTO driver (car_id, user_id, price)  Values (?,?,?)";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query);) {
            stmt.setInt(1, carId);
            stmt.setInt(2, userId);
            stmt.setInt(3, price);
            stmt.execute();
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    @Deprecated
    public void testQuerry() throws SQLException, ClassNotFoundException, ApplicationException {
        String query = "SELECT * from driver";
        Connection con = getConnection();
        try (PreparedStatement stmt = con.prepareStatement(query); ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
            con.commit();
        } catch (SQLException e) {
            rollback(con);
            throw new ApplicationException("DatabaseError", e);
        } finally {
            close(con);
        }
    }
    private void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                System.out.println("Cannot close connection" + e);
            }
        }
    }
    private void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.out.println("Cannot close resultSet" + e);
            }
        }
    }

    /**
     * rollback, if transaction was failed
     *
     * @param con Connection
     */

    private void rollback(Connection con) {
        if (con != null) {
            try {
                con.rollback();
            } catch (SQLException e) {
                System.out.println("Cannot rollback connection" + e);
            }
        }
    }
}
