package com.example.taxi.session;

import com.example.taxi.entity.Role;

public final class UserSession {
    private static UserSession instance;
    private String email;
    private Role role;

    private UserSession(String userName, Role role) {
        this.email = userName;
        this.role = role;
    }

    public static UserSession getInstance(String email, Role role) {
        if(instance == null) {
            instance = new UserSession(email, role);
        }
        else{
            instance.email = email;
            instance.role = role;
        }
        return instance;
    }
    public static UserSession getInstance() {
        if(instance == null) {
            instance = new UserSession("default", Role.CLIENT);
        }
        return instance;
    }

    public static String getEmail() {
        return instance.email;
    }

    public static Role getRole() {
        return instance.role;
    }

    public static void clear() {
        UserSession.instance.email = null;
        UserSession.instance.role = null;
    }

    @Override
    public String toString() {
        return "UserSession{" +
                "email='" + email + '\'' +
                ", role=" + role +
                '}';
    }
}
