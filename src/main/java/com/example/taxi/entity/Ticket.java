package com.example.taxi.entity;

public class Ticket {
    public int id;
    public User user;
    public String status;
    public Driver driver;
    public String from;
    public String to;
    public int feedback;
    public Ticket(int id, String status, User user, Driver driver, String from, String to, int feedback){
        this.id = id;
        this.status = status;
        this.user = user;
        this.driver = driver;
        this.from = from;
        this.to = to;
        this.feedback = feedback;
    }
    public Ticket(int id, String status, User user, String from, String to, int feedback){
        this.id = id;
        this.status = status;
        this.user = user;
        this.from = from;
        this.to = to;
        this.feedback = feedback;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getFeedback() {
        return feedback;
    }

    public void setFeedback(int feedback) {
        this.feedback = feedback;
    }
}
