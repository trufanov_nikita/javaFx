package com.example.taxi.entity;

import com.example.taxi.controller.DriverController;
import com.example.taxi.db.DBManager;
import com.example.taxi.exception.ApplicationException;
import com.example.taxi.session.UserSession;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;

import java.sql.SQLException;

public class TicketModel{
    SimpleIntegerProperty id = new SimpleIntegerProperty();
    SimpleStringProperty name = new SimpleStringProperty();
    SimpleStringProperty email = new SimpleStringProperty();
    SimpleStringProperty from = new SimpleStringProperty();
    SimpleStringProperty to = new SimpleStringProperty();
    SimpleStringProperty status = new SimpleStringProperty();
    Button button;
    public TicketModel(Ticket ticket) {
        this.id.set(ticket.getId());
        this.name.set(ticket.getUser().getName());
        this.email.set(ticket.getUser().getEmail());
        this.from.set(ticket.getFrom());
        this.to.set(ticket.getTo());
        this.status.set(ticket.getStatus());
        this.button = new Button();
    }
    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getFrom() {
        return from.get();
    }

    public SimpleStringProperty fromProperty() {
        return from;
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public String getTo() {
        return to.get();
    }

    public SimpleStringProperty toProperty() {
        return to;
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
}