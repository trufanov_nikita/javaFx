package com.example.taxi.entity;

import java.util.Objects;

public class Car {
    private int id;
    private String carNumber;
    private Boolean inRepair;

    public Car(int id, String carNumber, Boolean inRepair) {
        this.id = id;
        this.carNumber = carNumber;
        this.inRepair = inRepair;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return id == car.id && Objects.equals(carNumber, car.carNumber) && Objects.equals(inRepair, car.inRepair);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carNumber, inRepair);
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", carNumber='" + carNumber + '\'' +
                ", inRepair=" + inRepair +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public Boolean getInRepair() {
        return inRepair;
    }
}
