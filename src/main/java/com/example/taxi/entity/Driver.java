package com.example.taxi.entity;

import java.util.Objects;

public class Driver extends User{
    private Car car;
    private String status;
    private int price;
    private int starNumber;
    private float rating;

    public Driver(int id, String name, String email, Car car, String status, int price, int starNumber, float rating) {
        super(id, name, email, Role.DRIVER);
        this.car = car;
        this.status = status;
        this.price = price;
        this.starNumber = starNumber;
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driver driver = (Driver) o;
        return price == driver.price && starNumber == driver.starNumber && Float.compare(driver.rating, rating) == 0 && Objects.equals(car, driver.car) && Objects.equals(status, driver.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(car, status, price, starNumber, rating);
    }

    public Car getCar() {
        return car;
    }

    public String getStatus() {
        return status;
    }

    public int getPrice() {
        return price;
    }

    public int getStarNumber() {
        return starNumber;
    }

    public float getRating() {
        return rating;
    }
}
