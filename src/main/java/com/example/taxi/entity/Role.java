package com.example.taxi.entity;

public enum Role {
    CLIENT,
    ADMIN,
    DRIVER
}
